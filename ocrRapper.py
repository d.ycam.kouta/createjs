from PIL import Image
import sys

import pyocr
import pyocr.builders

# deguchi: ！この処理たさないとエラーになる
pyocr.tesseract.TESSERACT_CMD = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# 使用できるツールの一覧
tools = pyocr.get_available_tools()
if len(tools) == 0:
 print("No OCR tool found")
 sys.exit(1)
tool = tools[0]
print("Will use tool '%s'" % (tool.get_name()))

# 処理対象の言語一覧
langs = tool.get_available_languages()
print("Available languages: %s" % ", ".join(langs))
lang = langs[0]
print("Will use lang '%s'" % (lang))

# 画像読み込み
txt = tool.image_to_string(
# 読み込みたい画像を指定する
 Image.open('test5.png'),
 lang=lang,
 builder=pyocr.builders.TextBuilder()
)
# txt is a Python string

print(txt)